
let students = [];

function addStudent(name){
	studentNames = students.push(name);
	console.log(name + " was added on a student's list.");
}

function countStudents(){
	console.log("There are total of "+ students.length + " student/s enrolled.");
}

/*function printStudents(){
	students.sort().forEach(function(name){
		console.log(name);
	})
}*/

// Case-insensitive sort an array
function printStudents(){
	students.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase())).forEach(function(name){
		console.log(name);
	});
}

function findStudent(name) {
	names = students.filter(function(student){
		return student.toLowerCase().includes(name.toLowerCase())});

	if(names.length === 0) {
	console.log('No student found with the name ' + name + '.');
	}
	else if(names.length === 1) {
		console.log(names + ' is an enrollee.');
	}
	else {
		console.log(names.join(', ') + ' are enrollees.');
	}
}

